# latex-template


## Description

A LaTeX template for report-style documents. It is based on the
`KOMA` script class `scrreport` and incorporates some of the important
features of technical or scientific reports, such as:

- bibliography: `biblatex` package and the file bibliography.bib.
- acronym list: `glossaries` package and the file acronyms.sty.
- Linked references: `hyperref` package.
- Proper treatment of physical units: `siunitx` package.
- Proper tables: `booktabs` and `tabularx` package.


## Installation

To use the template, a modern LaTeX distribution is needed. The
template was tested with `texlive 2021`, but also other distributions
should work.


## Usage

A complete (initial) run should include the following:

```bash
pdflatex example.tex
biber example
makeglossaries example
pdflatex example.tex
pdflatex example.tex
```

```bash
biber example
```
is subsequently only needed when new entries are added to the bibliography file bibliography.bib, and

```bash
makeglossaries example
```
only when new entries are added to the acronyms file acronyms.sty.


## Contributing

If you find any errors, please report them by adding a new issue to this project.


## Authors and acknowledgment

This template is based on a largely simplified version of the Wegener
Center LaTeX framework, modified by Lukas Brunner and further
reduced by Florian Ladstäder.
